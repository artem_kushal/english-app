import React from 'react';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import EnglishApp from './containers/app-container';
import * as reducers from './reducers';

const reducer = combineReducers(reducers);
const store = createStore(reducer, applyMiddleware(thunk));

export default () => (
    <Provider store={store}>
        <EnglishApp />
    </Provider>
);
