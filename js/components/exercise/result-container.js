import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

import Button from '../common/button';

import FadeInView from '../common/fade-in-view';
import BounceView from '../common/bounce-view';

import { DARK_PRIMARY_COLOR, PRIMARY_COLOR } from '../../constants/palette';

const BtnStyle = {
    flex: 1,
    padding: 20,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: PRIMARY_COLOR,
    },
    mainContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    circle: {
        width: 150,
        height: 150,
        borderRadius: 75,
        marginBottom: 30,
        backgroundColor: DARK_PRIMARY_COLOR,
    },
    circleIcon: {
        lineHeight: 105,
        textAlign: 'center',
    },
    resultText: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
    },
    btnContainer: {
        flexDirection: 'row',
    },
    btn: {
        flex: 1,
        padding: 20,
    },
    leftBtn: {
        ...BtnStyle,
        marginRight: 4,
    },
    rightBtn: {
        ...BtnStyle,
        marginLeft: 4,
    },
    btnText: {
        textAlign: 'center',
    },
});

const ResultContainer = ({
    onCancel, onMore, total, wrongCount,
}) => (
    <FadeInView>
        <View style={styles.container}>
            <View style={styles.mainContent}>
                <BounceView>
                    <View style={styles.circle}>
                        <Icon style={styles.circleIcon} size={64} name="thumb-up" color="white" />
                    </View>
                </BounceView>

                <Text style={styles.resultText}>Всего: {total}</Text>
                <Text style={styles.resultText}>Правильных ответов: {total - wrongCount}</Text>
                <Text style={styles.resultText}>Неправильных ответов: {wrongCount}</Text>
            </View>

            <View style={styles.btnContainer}>
                <Button
                    style={styles.leftBtn}
                    backgroundColor={DARK_PRIMARY_COLOR}
                    onPress={() => onMore()}
                    value="ЕЩЕ"
                    textStyle={styles.btnText}
                />

                <Button
                    style={styles.rightBtn}
                    backgroundColor={DARK_PRIMARY_COLOR}
                    onPress={() => onCancel()}
                    value="ОК"
                    textStyle={styles.btnText}
                />
            </View>
        </View>
    </FadeInView>
);

ResultContainer.propTypes = {
    onCancel: PropTypes.func.isRequired,
    onMore: PropTypes.func.isRequired,
    total: PropTypes.number.isRequired,
    wrongCount: PropTypes.number.isRequired,
};

export default ResultContainer;
