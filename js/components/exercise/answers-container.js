import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { StyleSheet, View, Text } from 'react-native';

import { generateUniqRandList } from '../../tools/list-tools';
import Button from '../common/button';

import { MIN_WORDS_TO_EXERSIZE } from '../../constants/app-constants';

const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        marginBottom: 15,
    },
    label: {
        paddingTop: 20,
        paddingBottom: 30,
        color: 'grey',
        fontSize: 16,
    },
});

const normalBtnStyle = StyleSheet.create({
    btn: { backgroundColor: 'white' },
    text: { color: 'grey' },
});

const correctBtnStyle = StyleSheet.create({
    btn: { backgroundColor: 'green' },
    text: { color: 'white' },
});

const wrongBtnStyle = StyleSheet.create({
    btn: { backgroundColor: 'red' },
    text: { color: 'white' },
});

const getState = ({ words, currentWordId }) => {
    const correctItemIndex = words.findIndex(w => w.get('id') === currentWordId);
    let randList = generateUniqRandList(words.remove(correctItemIndex), MIN_WORDS_TO_EXERSIZE);

    randList = randList.set(
        Math.floor(Math.random() * MIN_WORDS_TO_EXERSIZE),
        words.get(correctItemIndex),
    );

    return {
        variantsToAnswer: randList,
        correctAnswerId: currentWordId,
        userAnswerId: null,
    };
};

class AnswerContainer extends Component {
    static propTypes = {
        words: PropTypes.instanceOf(List).isRequired,
        currentWordId: PropTypes.number.isRequired,
        onAddWrongAnswer: PropTypes.func.isRequired,
    };

    state = getState(this.props);

    componentWillReceiveProps(nextProps) {
        if (this.props.currentWordId !== nextProps.currentWordId) {
            this.setState(getState(nextProps));
        }
    }

    onGetAnswer(answerId) {
        if (this.state.userAnswerId) {
            return;
        }

        if (answerId !== this.state.correctAnswerId) {
            this.props.onAddWrongAnswer();
        }

        this.setState({
            userAnswerId: answerId,
        });
    }

    render() {
        const { correctAnswerId, variantsToAnswer, userAnswerId } = this.state;

        return (
            <View style={styles.container}>
                <Text style={styles.label}>ВЫБЕРИТЕ ПРАВИЛЬНЫЙ ПЕРЕВОД</Text>
                {variantsToAnswer.map((variant, i) => {
                    let btnStyle = normalBtnStyle;

                    if (userAnswerId) {
                        btnStyle =
                            variant.get('id') === correctAnswerId ? correctBtnStyle : btnStyle;
                        btnStyle =
                            userAnswerId === variant.get('id') && userAnswerId !== correctAnswerId
                                ? wrongBtnStyle
                                : btnStyle;
                    }

                    return (
                        <Button
                            key={i}
                            style={StyleSheet.flatten([styles.btn, btnStyle.btn])}
                            onPress={() => this.onGetAnswer(variant.get('id'))}
                            rippleColor="rgba(211, 211, 211, 0.75)"
                            value={variant.translation}
                            textStyle={btnStyle.text}
                        />
                    );
                })}
            </View>
        );
    }
}

export default AnswerContainer;
