import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Iterable } from 'immutable';

import { StyleSheet, View, StatusBar, Text } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import FabButton from '../common/fab-button';
import AnswersContainer from './answers-container';
import ResultContainer from './result-container';
import MinWordWarningContainer from './min-word-warning-container';

import { DARK_PRIMARY_COLOR, PRIMARY_COLOR, ACCENT_COLOR } from '../../constants/palette';

import { MIN_WORDS_TO_EXERSIZE } from '../../constants/app-constants';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    toolbar: {
        height: 56,
    },
    toolbarBottom: {
        height: 100,
        padding: 20,
        paddingBottom: 40,
        paddingTop: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
    },
    headerText: {
        color: 'white',
        fontSize: 21,
        fontWeight: 'bold',
    },
    headerTextNumber: {
        color: 'white',
        fontSize: 16,
    },
    fabBtn: {
        position: 'absolute',
        right: 10,
        top: 126,
    },
});

export default class ExerciseScreen extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,

        words: PropTypes.instanceOf(Iterable).isRequired,
        getWords: PropTypes.func.isRequired,

        navigation: PropTypes.object.isRequired,
    };

    state = {
        currentWordNumber: 1,
        wrongAnswersCount: 0,
    };

    componentWillMount() {
        const { categoryId } = this.props.navigation.state.params;

        this.props.getWords(categoryId);
    }

    onNextWord = () => {
        this.setState(prevState => ({
            currentWordNumber: prevState.currentWordNumber + 1,
        }));
    };

    onOneMoreAttempt = () =>
        this.setState({
            currentWordNumber: 1,
            wrongAnswersCount: 0,
        });

    onAddWrongAnswer = () => this.setState({ wrongAnswersCount: this.state.wrongAnswersCount + 1 });

    onCancel = () => {
        this.props.navigation.goBack();
    };

    render() {
        const { title, words } = this.props;
        const { currentWordNumber, wrongAnswersCount } = this.state;
        const currentWord = words.get(currentWordNumber - 1);

        if (!words.count()) {
            return null;
        }

        if (words.count() < MIN_WORDS_TO_EXERSIZE) {
            return <MinWordWarningContainer onCancel={this.onCancel} />;
        }

        if (currentWordNumber > words.count()) {
            return (
                <ResultContainer
                    onCancel={this.onCancel}
                    onMore={this.onOneMoreAttempt}
                    total={words.count()}
                    wrongCount={wrongAnswersCount}
                />
            );
        }

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={DARK_PRIMARY_COLOR} />
                <Icon.ToolbarAndroid
                    backgroundColor={PRIMARY_COLOR}
                    style={styles.toolbar}
                    title={title}
                    titleColor="white"
                    navIconName="arrow-back"
                    onIconClicked={() => this.props.navigation.goBack()}
                />
                <View style={styles.toolbarBottom} backgroundColor={PRIMARY_COLOR}>
                    <Text style={styles.headerText}>
                        {currentWord ? currentWord.text.toUpperCase() : ''}
                    </Text>
                    <Text style={styles.headerTextNumber}>
                        {currentWordNumber}/{words.count()}
                    </Text>
                </View>

                <AnswersContainer
                    words={words}
                    currentWordId={words.get(currentWordNumber - 1).get('id')}
                    onAddWrongAnswer={this.onAddWrongAnswer}
                />

                <FabButton
                    backgroundColor={ACCENT_COLOR}
                    style={styles.fabBtn}
                    iconName="keyboard-arrow-right"
                    onPress={this.onNextWord}
                />
            </View>
        );
    }
}
