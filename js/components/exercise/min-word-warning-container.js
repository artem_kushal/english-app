import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

import Button from '../common/button';

import FadeInView from '../common/fade-in-view';
import BounceView from '../common/bounce-view';

import { DARK_PRIMARY_COLOR, PRIMARY_COLOR } from '../../constants/palette';

import { MIN_WORDS_TO_EXERSIZE } from '../../constants/app-constants';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: PRIMARY_COLOR,
    },
    mainContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    circle: {
        width: 150,
        height: 150,
        borderRadius: 75,
        marginBottom: 30,
        backgroundColor: DARK_PRIMARY_COLOR,
    },
    circleIcon: {
        lineHeight: 105,
        textAlign: 'center',
    },
    warningText: {
        width: 300,
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    btn: {
        flex: 1,
        padding: 20,
        backgroundColor: 'white',
    },
    btnText: {
        textAlign: 'center',
    },
});

const ResultContainer = ({ onCancel }) => (
    <FadeInView>
        <View style={styles.container}>
            <View style={styles.mainContent}>
                <BounceView>
                    <View style={styles.circle}>
                        <Icon style={styles.circleIcon} size={64} name="warning" color="white" />
                    </View>
                </BounceView>
                <Text style={styles.warningText}>
                    Количесто слов должно быть не меньше {MIN_WORDS_TO_EXERSIZE}
                </Text>
            </View>

            <Button
                style={styles.btnContainer}
                backgroundColor={DARK_PRIMARY_COLOR}
                onPress={() => onCancel()}
                value="ОК"
                textStyle={styles.btnText}
            />
        </View>
    </FadeInView>
);

ResultContainer.propTypes = {
    onCancel: PropTypes.func.isRequired,
};

export default ResultContainer;
