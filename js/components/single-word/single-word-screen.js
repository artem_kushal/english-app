import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { StyleSheet, View, StatusBar, ToastAndroid } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { MKTextField } from 'react-native-material-kit';
import Button from '../common/button';

import {
    DARK_PRIMARY_COLOR,
    PRIMARY_COLOR,
    SECONDARY_TEXT_COLOR,
    ACCENT_COLOR,
} from '../../constants/palette';

const floatingLabelFont = {
    fontSize: 14,
    fontStyle: 'italic',
    fontWeight: '200',
};

const ERROR_COLOR = 'red';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    contentContainer: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 15,
        marginRight: 15,
    },
    textfield: {
        height: 48,
        marginTop: 10,
    },
    toolbar: {
        height: 56,
    },
    btn: {
        marginTop: 30,
    },
});

export default class NewWordScreen extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,

        wordId: PropTypes.number,
        text: PropTypes.string,
        translation: PropTypes.string,
        transcription: PropTypes.string,

        toastText: PropTypes.string.isRequired,
        saveWord: PropTypes.func.isRequired,

        navigation: PropTypes.object.isRequired,
    };

    static defaultProps = {
        wordId: null,
        text: '',
        translation: '',
        transcription: '',
    };

    state = {
        text: this.props.text,
        translation: this.props.translation,
        transcription: this.props.transcription,
        isTextValid: true,
        isTranslationValid: true,
    };

    onChangeText(newState) {
        this.setState(newState);
    }

    onClickAddBtn = () => {
        const { text, translation, transcription } = this.state;

        if (text === '' || translation === '') {
            this.setState({
                isTextValid: !text === '',
                isTranslationValid: !translation === '',
            });

            ToastAndroid.show('Пожалуйста заполните основные данные.', ToastAndroid.SHORT);
        } else {
            this.props.saveWord(this.props.wordId, text, translation, transcription);
            this.props.navigation.goBack();

            ToastAndroid.show(this.props.toastText, ToastAndroid.SHORT);
        }
    };

    render() {
        const { title, navigation } = this.props;
        const { isTextValid, isTranslationValid } = this.state;

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={DARK_PRIMARY_COLOR} />
                <Icon.ToolbarAndroid
                    backgroundColor={PRIMARY_COLOR}
                    style={styles.toolbar}
                    title={title}
                    titleColor="white"
                    navIconName="arrow-back"
                    onIconClicked={() => navigation.goBack()}
                />

                <View style={styles.contentContainer}>
                    <MKTextField
                        floatingLabelEnabled
                        tintColor={isTextValid ? SECONDARY_TEXT_COLOR : ERROR_COLOR}
                        highlightColor={isTextValid ? ACCENT_COLOR : ERROR_COLOR}
                        textInputStyle={{ color: SECONDARY_TEXT_COLOR }}
                        placeholder="Слово"
                        style={styles.textfield}
                        floatingLabelFont={floatingLabelFont}
                        value={this.state.text}
                        onChange={e =>
                            this.onChangeText({
                                text: e.nativeEvent.text,
                            })
                        }
                    />
                    <MKTextField
                        floatingLabelEnabled
                        tintColor={isTranslationValid ? SECONDARY_TEXT_COLOR : ERROR_COLOR}
                        highlightColor={isTranslationValid ? ACCENT_COLOR : ERROR_COLOR}
                        textInputStyle={{ color: SECONDARY_TEXT_COLOR }}
                        placeholder="Перевод"
                        style={styles.textfield}
                        floatingLabelFont={floatingLabelFont}
                        value={this.state.translation}
                        onChange={e =>
                            this.onChangeText({
                                translation: e.nativeEvent.text,
                            })
                        }
                    />
                    <MKTextField
                        floatingLabelEnabled
                        tintColor={SECONDARY_TEXT_COLOR}
                        highlightColor={ACCENT_COLOR}
                        textInputStyle={{ color: SECONDARY_TEXT_COLOR }}
                        placeholder="Транскрипция"
                        style={styles.textfield}
                        floatingLabelFont={floatingLabelFont}
                        value={this.state.transcription}
                        onChange={e =>
                            this.onChangeText({
                                transcription: e.nativeEvent.text,
                            })
                        }
                    />

                    <Button
                        style={styles.btn}
                        backgroundColor={DARK_PRIMARY_COLOR}
                        onPress={this.onClickAddBtn}
                        value="Сохранить"
                    />
                </View>
            </View>
        );
    }
}
