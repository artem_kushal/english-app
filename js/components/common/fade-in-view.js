import React, { Component } from 'react';
import { Animated } from 'react-native';

export default class FadeInView extends Component {
    state = {
        fadeAnim: new Animated.Value(0),
    };

    componentDidMount() {
        Animated.timing(this.state.fadeAnim, { toValue: 1 }).start();
    }

    render() {
        return (
            <Animated.View
                style={{
                    flex: 1,
                    opacity: this.state.fadeAnim,
                }}
            >
                {this.props.children}
            </Animated.View>
        );
    }
}
