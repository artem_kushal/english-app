import React from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet } from 'react-native';
import Menu, { MenuOptions, MenuOption, MenuTrigger } from 'react-native-menu';

const menuStyles = StyleSheet.create({
    triggerText: {
        fontSize: 16,
        paddingBottom: 8,
        minHeight: 28,
    },
    optionsContainer: {
        width: 252,
        paddingTop: 8,
        paddingBottom: 8,
    },
    option: {
        paddingLeft: 16,
        paddingRight: 16,
        height: 48,
    },
    optionText: {
        fontSize: 16,
    },
});

const DropdownMenu = ({ options, onSelectionChange, selectedValue }) => {
    const selectedLabel = options.find(option => option.value === selectedValue).label;

    return (
        <Menu onSelect={value => onSelectionChange(value)} style={{ flex: 1 }}>
            <MenuTrigger>
                <Text style={menuStyles.triggerText}>{selectedLabel}</Text>
            </MenuTrigger>
            <MenuOptions optionsContainerStyle={menuStyles.optionsContainer}>
                {options.map((option, i) => (
                    <MenuOption style={menuStyles.option} key={i} value={option.value}>
                        <Text style={menuStyles.optionText}>{option.label}</Text>
                    </MenuOption>
                ))}
            </MenuOptions>
        </Menu>
    );
};

DropdownMenu.propTypes = {
    onSelectionChange: PropTypes.func.isRequired,
    selectedValue: PropTypes.any.isRequired,
    options: PropTypes.array.isRequired,
};

export default DropdownMenu;
