import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Animated } from 'react-native';

class BounceView extends Component {
    state = {
        bounceValue: new Animated.Value(0),
    };

    componentDidMount() {
        this.state.bounceValue.setValue(1.5);

        Animated.spring(this.state.bounceValue, {
            toValue: 1,
            friction: 1,
        }).start();
    }

    render() {
        return (
            <Animated.View
                style={{
                    transform: [{ scale: this.state.bounceValue }],
                }}
            >
                {this.props.children}
            </Animated.View>
        );
    }
}

export default BounceView;
