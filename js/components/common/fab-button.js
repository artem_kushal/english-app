import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import { MKButton } from 'react-native-material-kit';

const styles = StyleSheet.create({
    fabBtn: {
        justifyContent: 'center',
        width: 60,
        height: 60,
        borderRadius: 30,
        shadowRadius: 1,
        shadowOffset: { width: 0, height: 0.5 },
        shadowOpacity: 0.7,
        shadowColor: 'black',
        elevation: 4,
    },
    fabBtnIcon: {
        textAlign: 'center',
    },
});

const FabButton = (props) => {
    const {
        iconName, style, iconStyle, onPress, backgroundColor,
    } = props;
    const btnStyle = StyleSheet.flatten([styles.fabBtn, style]);
    const iconBtnStyle = StyleSheet.flatten([styles.fabBtnIcon, iconStyle]);

    return (
        <MKButton
            backgroundColor={backgroundColor}
            onPress={() => onPress()}
            style={btnStyle}
            rippleLocation="center"
        >
            <Icon name={iconName} color="white" size={22} style={iconBtnStyle} />
        </MKButton>
    );
};

FabButton.propTypes = {
    iconName: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    backgroundColor: PropTypes.string.isRequired,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    iconStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
};

FabButton.defaultProps = {
    style: {},
    iconStyle: {},
};

export default FabButton;
