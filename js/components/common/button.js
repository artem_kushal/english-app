import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text } from 'react-native';

import { MKButton } from 'react-native-material-kit';

const styles = StyleSheet.create({
    baseBtn: {
        padding: 15,
        borderRadius: 2,
        shadowRadius: 1,
        shadowOffset: { width: 0, height: 0.5 },
        shadowOpacity: 0.7,
        shadowColor: 'black',
        elevation: 4,
    },
    baseBtnText: {
        color: 'white',
        fontWeight: 'bold',
    },
});

const Button = (props) => {
    const {
        backgroundColor, onPress, style, textStyle, rippleColor, value,
    } = props;
    const btnTextStyle = StyleSheet.flatten([styles.baseBtnText, textStyle]);
    let btnStyle = StyleSheet.flatten([styles.baseBtn, style]);

    if (backgroundColor) {
        btnStyle = StyleSheet.flatten([
            btnStyle,
            {
                backgroundColor,
            },
        ]);
    }

    return (
        <MKButton onPress={() => onPress()} style={btnStyle} rippleColor={rippleColor}>
            <Text pointerEvents="none" style={btnTextStyle}>
                {value}
            </Text>
        </MKButton>
    );
};

Button.propTypes = {
    onPress: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
    backgroundColor: PropTypes.string,
    rippleColor: PropTypes.string,
    style: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
    textStyle: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
};

Button.defaultProps = {
    backgroundColor: null,
    style: {},
    textStyle: {},
};

export default Button;
