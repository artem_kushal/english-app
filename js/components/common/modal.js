import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    BackHandler,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native';

import { MKButton } from 'react-native-material-kit';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.75)',
    },
    innerContainer: {
        width: 300,
        backgroundColor: 'white',
    },
    modalBody: {
        margin: 24,
    },
    modalTitle: {
        color: '#212121',
        paddingBottom: 20,
        fontSize: 20,
    },
    modalActions: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        padding: 8,
    },
    flatBtn: {
        height: 36,
        minWidth: 64,
        paddingLeft: 8,
        paddingRight: 8,
        marginLeft: 8,
    },
    flatBtnText: {
        lineHeight: 28,
        textAlign: 'center',
    },
});

const FlatButton = MKButton.flatButton().build();

class CustomModal extends Component {
    static propTypes = {
        isVisible: PropTypes.bool.isRequired,
        onCancel: PropTypes.func.isRequired,
        onSuccess: PropTypes.func.isRequired,
        titleText: PropTypes.string.isRequired,
        successBtnText: PropTypes.string,
        cancelBtnText: PropTypes.string,
    };

    static defaultProps = {
        successBtnText: 'ок',
        cancelBtnText: 'отмена',
    };

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.hardwareBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.hardwareBackPress);
    }

    hardwareBackPress = () => {
        if (this.props.isVisible) {
            this.props.onCancel();
            return true;
        }
        return false;
    };

    render() {
        const {
            isVisible,
            titleText,
            successBtnText,
            cancelBtnText,
            onSuccess,
            onCancel,
        } = this.props;

        return (
            <Modal animationType="slide" transparent visible={isVisible} onRequestClose={() => {}}>
                <TouchableOpacity
                    style={styles.container}
                    activeOpacity={1}
                    onPressOut={() => onCancel()}
                >
                    <TouchableWithoutFeedback>
                        <View style={styles.innerContainer} elevation={5}>
                            <View style={styles.modalBody}>
                                <Text style={styles.modalTitle}>{titleText}</Text>
                                {this.props.children}
                            </View>

                            <View style={styles.modalActions}>
                                <FlatButton style={styles.flatBtn} onPress={() => onSuccess()}>
                                    <Text style={styles.flatBtnText}>
                                        {successBtnText.toUpperCase()}
                                    </Text>
                                </FlatButton>
                                <FlatButton style={styles.flatBtn} onPress={() => onCancel()}>
                                    <Text style={styles.flatBtnText}>
                                        {cancelBtnText.toUpperCase()}
                                    </Text>
                                </FlatButton>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </TouchableOpacity>
            </Modal>
        );
    }
}

export default CustomModal;
