import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { StyleSheet, View, Text, TouchableNativeFeedback } from 'react-native';
import { MKButton } from 'react-native-material-kit';

const ColoredFlatButton = MKButton.flatButton().build();

const styles = StyleSheet.create({
    card: {
        marginTop: 10,
        width: 160,
        height: 140,
        justifyContent: 'flex-end',
    },
    cardBody: {
        flex: 1,
        padding: 16,
        justifyContent: 'flex-end',
    },
    bodyText: {
        fontSize: 18,
        color: 'white',
    },
    cardActions: {
        padding: 8,
        flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
    },
});

const CategoryCard = ({
    categoryId,
    categoryName,
    onCardBodyClick,
    backgroundColor,
    onRemove,
    onEdit,
    onStudy,
}) => {
    const customStyle = { backgroundColor };

    return (
        <View style={[styles.card, customStyle]} elevation={5}>
            <TouchableNativeFeedback
                onPress={() => onCardBodyClick(categoryId, categoryName)}
                background={TouchableNativeFeedback.Ripple('black')}
            >
                <View style={styles.cardBody}>
                    <Text style={styles.bodyText}>{categoryName}</Text>
                </View>
            </TouchableNativeFeedback>

            <View style={styles.cardActions}>
                <ColoredFlatButton onPress={() => onRemove(categoryId)}>
                    <Icon size={18} name="close" color="grey" />
                </ColoredFlatButton>
                <ColoredFlatButton onPress={() => onEdit(categoryId)}>
                    <Icon size={18} name="mode-edit" color="grey" />
                </ColoredFlatButton>
                <ColoredFlatButton onPress={() => onStudy(categoryId)}>
                    <Icon size={18} name="school" color="grey" />
                </ColoredFlatButton>
            </View>
        </View>
    );
};

CategoryCard.propTypes = {
    categoryId: PropTypes.number.isRequired,
    categoryName: PropTypes.string.isRequired,
    onCardBodyClick: PropTypes.func.isRequired,
    backgroundColor: PropTypes.string,
    onRemove: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
    onStudy: PropTypes.func.isRequired,
};

CategoryCard.defaultProps = {
    backgroundColor: '#009688',
};

export default CategoryCard;
