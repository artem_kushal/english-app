import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Iterable } from 'immutable';
import { StyleSheet, View, StatusBar, Alert } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import CategoryCard from './category-card';
import EditCategoryModal from '../../containers/edit-category-modal-container';
import AddCategoryModal from '../../containers/add-category-modal-container';

import { RoutesID } from '../../constants/navigation-constants';

import { DARK_PRIMARY_COLOR, PRIMARY_COLOR } from '../../constants/palette';

const DARK_MODAL_COLOR = '#061f38';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    toolbar: {
        height: 56,
    },
    contentContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        padding: 10,
        paddingTop: 0,
        justifyContent: 'space-between',
    },
});

export default class CategoryScreen extends Component {
    static propTypes = {
        categories: PropTypes.instanceOf(Iterable).isRequired,

        onRemoveCategory: PropTypes.func.isRequired,
        onStartEditingCategory: PropTypes.func.isRequired,

        title: PropTypes.string.isRequired,
        navigation: PropTypes.object.isRequired,
    };

    state = {
        isAddModalVisible: false,
        isEditModalVisible: false,
    };

    onOpenWordList = (categoryId, categoryName) => {
        const { navigation } = this.props;

        navigation.navigate(RoutesID.WORD_LIST, {
            categoryId,
            title: categoryName,
        });
    };

    onToggleAddModal = () =>
        this.setState({
            isAddModalVisible: !this.state.isAddModalVisible,
        });

    onOpenEditModal = (categoryId) => {
        this.props.onStartEditingCategory(categoryId);

        this.setState({
            isEditModalVisible: true,
        });
    };

    onCloseEditModal = () =>
        this.setState({
            isEditModalVisible: false,
        });

    onRemoveCategory = (categoryId) => {
        Alert.alert('Удаление категории', 'Вы действительно хотите удалить категорию?', [
            { text: 'ОТМЕНА', onPress: () => {}, style: 'cancel' },
            { text: 'ОК', onPress: () => this.props.onRemoveCategory(categoryId) },
        ]);
    };

    onStudyCategory = (categoryId) => {
        const { navigation } = this.props;

        navigation.navigate(RoutesID.EXERCISE, {
            categoryId,
        });
    };

    render() {
        const { title, categories, navigation } = this.props;
        const { isAddModalVisible, isEditModalVisible } = this.state;
        const categoryCards = categories.map((category, i) => (
            <CategoryCard
                key={i}
                categoryId={category.id}
                categoryName={category.name}
                onCardBodyClick={this.onOpenWordList}
                backgroundColor={category.color}
                onRemove={this.onRemoveCategory}
                onEdit={this.onOpenEditModal}
                onStudy={this.onStudyCategory}
            />
        ));
        const statusBarBgColor =
            isAddModalVisible || isEditModalVisible ? DARK_MODAL_COLOR : DARK_PRIMARY_COLOR;

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={statusBarBgColor} />
                <Icon.ToolbarAndroid
                    style={styles.toolbar}
                    title={title}
                    titleColor="white"
                    backgroundColor={PRIMARY_COLOR}
                    navIconName="menu"
                    onIconClicked={() => navigation.navigate('DrawerToggle')}
                    actions={[{ title: 'Add category', iconName: 'add-circle', show: 'always' }]}
                    onActionSelected={this.onToggleAddModal}
                />
                <View style={styles.contentContainer}>{categoryCards}</View>

                {isAddModalVisible && <AddCategoryModal onClose={this.onToggleAddModal} />}
                {isEditModalVisible && <EditCategoryModal onClose={this.onCloseEditModal} />}
            </View>
        );
    }
}

// static navigationOptions = ({ navigation }) => {
//     const { params = {} } = navigation.state;

//     return {
//         headerStyle: {
//             backgroundColor: params.headerBgColor,
//         },
//     };
// };

// componentDidMount() {
//     this.props.navigation.setParams({
//         headerBgColor: this.props.theme.PRIMARY_COLOR,
//     });
// }
