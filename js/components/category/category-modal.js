import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet } from 'react-native';

import { MKTextField } from 'react-native-material-kit';

import Modal from '../common/modal';
import ColorPicker from './color-picker';

import { CATEGORY_COLOR } from '../../constants/category-constants';
import { ACCENT_COLOR, SECONDARY_TEXT_COLOR } from '../../constants/palette';

const NOT_VALID_COLOR = 'red';

const styles = StyleSheet.create({
    dropdownLabel: {
        paddingBottom: 8,
        fontSize: 12,
    },
    textfieldLabel: {
        fontSize: 12,
        paddingTop: 8,
        paddingBottom: 8,
    },
    textfield: {
        height: 28,
    },
});

class CategoryModal extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,

        name: PropTypes.string,
        color: PropTypes.string,
        onSaveCategory: PropTypes.func.isRequired,

        onClose: PropTypes.func.isRequired,
    };

    static defaultProps = {
        name: null,
        color: CATEGORY_COLOR[0],
    };

    state = {
        selectedColor: this.props.color,
        name: this.props.name,
        isValid: true,
    };

    onSelectionColorChange = (newColor) => {
        this.setState({ selectedColor: newColor });
    };

    onChangeText(text) {
        this.setState({ name: text });
    }

    onSuccess = () => {
        const { selectedColor, name } = this.state;

        if (name) {
            this.props.onSaveCategory(name, selectedColor);
            this.props.onClose();
        } else {
            this.setState({
                isValid: false,
            });
        }
    };

    render() {
        const { selectedColor, isValid } = this.state;

        return (
            <Modal
                isVisible
                onSuccess={this.onSuccess}
                onCancel={this.props.onClose}
                titleText={this.props.title}
                successBtnText="Сохранить"
            >
                <Text style={styles.dropdownLabel}>Выберите цвет</Text>
                <ColorPicker
                    colors={CATEGORY_COLOR}
                    selectedColor={selectedColor}
                    accentColor={ACCENT_COLOR}
                    onChangeSelection={this.onSelectionColorChange}
                />

                <Text style={styles.textfieldLabel}>Введите название</Text>
                <MKTextField
                    tintColor={isValid ? SECONDARY_TEXT_COLOR : NOT_VALID_COLOR}
                    highlightColor={isValid ? ACCENT_COLOR : NOT_VALID_COLOR}
                    textInputStyle={{ color: SECONDARY_TEXT_COLOR }}
                    placeholder="Новая категория"
                    style={styles.textfield}
                    value={this.state.name}
                    onChange={e => this.onChangeText(e.nativeEvent.text)}
                />
            </Modal>
        );
    }
}

export default CategoryModal;
