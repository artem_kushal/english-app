import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { MKButton } from 'react-native-material-kit';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    colorCircle: {
        justifyContent: 'center',
        margin: 5,
        width: 40,
        height: 40,
        borderRadius: 20,
        shadowRadius: 1,
        shadowOffset: { width: 0, height: 0.5 },
        shadowOpacity: 0.4,
        shadowColor: 'black',
        elevation: 4,
    },
    activeColorCircle: {
        width: 32,
        height: 32,
        borderRadius: 16,
        margin: 2,
    },
    activeCircleContainer: {
        margin: 5,
        width: 40,
        height: 40,
        borderRadius: 20,
        borderWidth: 2,
    },
});

class ColorPicker extends Component {
    static propTypes = {
        colors: PropTypes.array.isRequired,
        selectedColor: PropTypes.string.isRequired,
        accentColor: PropTypes.string.isRequired,
        onChangeSelection: PropTypes.func.isRequired,
    };

    getColorCircle = (isActive, color, key) => {
        const circleStyles = isActive
            ? styles.colorCircle
            : StyleSheet.flatten([styles.colorCircle, styles.activeColorCircle]);

        return (
            <MKButton
                key={key}
                backgroundColor={color}
                onPress={() => this.props.onChangeSelection(color)}
                style={circleStyles}
                rippleLocation="center"
            />
        );
    };

    render() {
        const { colors, accentColor, selectedColor } = this.props;
        const activeCircleContainer = StyleSheet.flatten([
            styles.activeCircleContainer,
            {
                borderColor: accentColor,
            },
        ]);

        return (
            <View style={styles.container}>
                {colors.map((color, i) => {
                    const isActive = color !== selectedColor;
                    const circleBtn = this.getColorCircle(isActive, color, i);

                    if (isActive) {
                        return circleBtn;
                    }
                    return (
                        <View style={activeCircleContainer} key={i}>
                            {circleBtn}
                        </View>
                    );
                })}
            </View>
        );
    }
}

export default ColorPicker;
