import React from 'react';
import PropTypes from 'prop-types';
import {
    StyleSheet,
    Text,
    View,
    TouchableNativeFeedback,
    TouchableWithoutFeedback,
} from 'react-native';

import { MKCheckbox } from 'react-native-material-kit';

import { PRIMARY_TEXT_COLOR, SECONDARY_TEXT_COLOR, ACCENT_COLOR } from '../../constants/palette';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 72,
        paddingLeft: 16,
    },
    textContainer: {
        justifyContent: 'center',
    },
    primaryText: {
        fontSize: 16,
        color: PRIMARY_TEXT_COLOR,
    },
    secondaryText: {
        fontSize: 16,
        color: SECONDARY_TEXT_COLOR,
    },
    rightActionContainer: {
        justifyContent: 'center',
        paddingHorizontal: 8,
    },
    checkbox: {
        padding: 0,
    },
});

const WordListItem = (props) => {
    const {
        id, primaryText, secondaryText, isSelected, onSelectItem, onClickItem,
    } = props;

    return (
        <TouchableNativeFeedback
            onPress={() => onClickItem(id)}
            background={TouchableNativeFeedback.Ripple('grey')}
        >
            <View style={styles.container}>
                <View style={styles.textContainer}>
                    <Text style={styles.primaryText}>{primaryText}</Text>
                    <Text style={styles.secondaryText}>{secondaryText}</Text>
                </View>
                <TouchableWithoutFeedback>
                    <View style={styles.rightActionContainer}>
                        <MKCheckbox
                            borderOnColor={ACCENT_COLOR}
                            fillColor={ACCENT_COLOR}
                            checked={isSelected}
                            onCheckedChange={() => onSelectItem(id)}
                        />
                    </View>
                </TouchableWithoutFeedback>
            </View>
        </TouchableNativeFeedback>
    );
};

WordListItem.propTypes = {
    id: PropTypes.number.isRequired,
    primaryText: PropTypes.string.isRequired,
    secondaryText: PropTypes.string.isRequired,
    isSelected: PropTypes.bool,
    onClickItem: PropTypes.func.isRequired,
    onSelectItem: PropTypes.func.isRequired,
};

WordListItem.defaultProps = {
    isSelected: false,
};

export default WordListItem;
