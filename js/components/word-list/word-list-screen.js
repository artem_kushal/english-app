import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Iterable, List } from 'immutable';
import { StyleSheet, View, FlatList, StatusBar, Alert } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import WordListItem from './word-list-item';
import FabButton from '../common/fab-button';
import { RoutesID } from '../../constants/navigation-constants';

import { DARK_PRIMARY_COLOR, PRIMARY_COLOR, ACCENT_COLOR } from '../../constants/palette';

const DELETE_ACTION = 0;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    fabBtn: {
        position: 'absolute',
        bottom: 10,
        right: 10,
    },
    toolbar: {
        height: 56,
    },
    actionBar: {
        height: 56,
    },
    icon: {
        color: 'darkgray',
    },
});

export default class WordListScreen extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        words: PropTypes.instanceOf(Iterable).isRequired,
        getWords: PropTypes.func.isRequired,
        deleteWords: PropTypes.func.isRequired,
        navigation: PropTypes.object.isRequired,
    };

    state = {
        selectedWordIds: new List(),
    };

    componentWillMount() {
        const { categoryId } = this.props.navigation.state.params;

        this.props.getWords(categoryId);
    }

    onPressAddBtn = () => {
        const { navigation } = this.props;

        navigation.navigate(RoutesID.NEW_WORD);
    };

    onActionBarNavIconClicked = () => {
        this.setState({ selectedWordIds: new List() });
    };

    onActionBarActionSelected = (actionIndex) => {
        if (actionIndex === DELETE_ACTION) {
            Alert.alert('Удаление слов', 'Вы действительно хотите удалить выбранные слова?', [
                { text: 'ОТМЕНА', onPress: () => {}, style: 'cancel' },
                { text: 'ОК', onPress: this.deleteWords },
            ]);
        }
    };

    onSelectedWord = (wordId) => {
        const { selectedWordIds } = this.state;
        const index = selectedWordIds.findIndex(id => id === wordId);

        this.setState({
            selectedWordIds:
                index === -1 ? selectedWordIds.push(wordId) : selectedWordIds.delete(index),
        });
    };

    onEditWord = (wordId) => {
        const { categoryId } = this.props.navigation.state.params;
        const { navigation } = this.props;

        navigation.navigate(RoutesID.EDIT_WORD, { categoryId, wordId });
    };

    deleteWords = () => {
        this.props.deleteWords(this.state.selectedWordIds);
        this.setState({ selectedWordIds: new List() });
    };

    render() {
        const { title: paramsTitle } = this.props.navigation.state.params;
        const { title, navigation } = this.props;
        const { selectedWordIds } = this.state;
        const isShowActionBar = selectedWordIds.count() > 0;
        const toolbar = (
            <Icon.ToolbarAndroid
                backgroundColor={PRIMARY_COLOR}
                style={styles.toolbar}
                title={paramsTitle || title}
                titleColor="white"
                navIconName="arrow-back"
                onIconClicked={() => navigation.goBack()}
            />
        );
        const actionBar = (
            <Icon.ToolbarAndroid
                backgroundColor={PRIMARY_COLOR}
                style={styles.actionBar}
                titleColor="white"
                navIconName="close"
                onIconClicked={this.onActionBarNavIconClicked}
                actions={[{ title: 'Delete', iconName: 'delete', show: 'always' }]}
                onActionSelected={this.onActionBarActionSelected}
            />
        );

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={DARK_PRIMARY_COLOR} />

                {isShowActionBar ? actionBar : toolbar}

                <FlatList
                    data={this.props.words.toArray()}
                    renderItem={({ item }) => (
                        <WordListItem
                            key={item.get('id')}
                            id={item.get('id')}
                            primaryText={item.get('text')}
                            secondaryText={item.get('translation')}
                            isSelected={selectedWordIds.includes(item.get('id'))}
                            onClickItem={this.onEditWord}
                            onSelectItem={this.onSelectedWord}
                        />
                    )}
                />

                <FabButton
                    backgroundColor={ACCENT_COLOR}
                    style={styles.fabBtn}
                    iconName="add"
                    onPress={this.onPressAddBtn}
                />
            </View>
        );
    }
}
