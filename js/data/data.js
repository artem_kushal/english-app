const simpleFood = [
    { text: 'grapes', translation: 'виноград' },
    { text: 'apple', translation: 'яблоко' },
    { text: 'water', translation: 'вода' },
    { text: 'bread', translation: 'хлеб' },
    { text: 'chicken', translation: 'курица' },
    { text: 'banana', translation: 'банан' },
    { text: 'eggs', translation: 'яйца' },
    { text: 'cheese', translation: 'сыр' },
    { text: 'tomatoes', translation: 'помидоры' },
    { text: 'sandwich', translation: 'бутерброд' },
    { text: 'olive oil', translation: 'оливковое масло' },
    { text: 'salad', translation: 'салат' },
    { text: 'biscuits', translation: 'печенье' },
    { text: 'orange juice', translation: 'апельсиновый сок' },
    { text: 'fruit', translation: 'фрукты' },
    { text: 'butter', translation: 'масло' },
    { text: 'ham', translation: 'ветчина' },
    { text: 'beef', translation: 'говядина' },
    { text: 'pork', translation: 'свинина' },
    { text: 'yougurt', translation: 'йогурт' },
    { text: 'potatoes', translation: 'картофель' },
    { text: 'pineapple', translation: 'ананас' },
    { text: 'vegetables', translation: 'овощи' },
    { text: 'cucumbers', translation: 'огурцы' },
    { text: 'carrots', translation: 'морковь' },
    { text: 'rice', translation: 'рис' },
    { text: 'pasta', translation: 'макароны' },
    { text: 'hot dogs', translation: 'хот-дог' },
    { text: 'hamburgers', translation: 'гамбургер' },
    { text: 'pizzas', translation: 'пицца' },
    { text: 'fish and chips', translation: 'рыба с жаренной картошкой' },
    { text: 'beans', translation: 'фасоль' },
    { text: 'peas', translation: 'горох' },
    { text: 'onions', translation: 'лук' },
    { text: 'garlic', translation: 'чеснок' },
    { text: 'mushrooms', translation: 'грибы' },
    { text: 'pear', translation: 'груша' },
    { text: 'strawberries', translation: 'клубника' },
    { text: 'wine', translation: 'вино' },
];

const difficultFood = [
    { text: 'a bag of shopping', translation: 'сумка с покупками' },
    { text: 'a box of chocolates', translation: 'коробка шоколада' },
    { text: 'chocolate bar', translation: 'шоколадный батончик(mars)' },
    { text: 'bar of chocolate', translation: 'плитка шоколада' },
    { text: 'a carton of juice', translation: 'картонная упаковка сока' },
    { text: 'a can of cola', translation: 'жестяная банка колы' },
    { text: 'a cup of coffee', translation: 'чашка кофе' },
    { text: 'a tube of toothpaste', translation: 'тюбик зубной пасты' },
    { text: 'a tin of fruit', translation: 'консервная банка фруктов' },
    { text: 'a bowl of sugar', translation: 'глубокая тарелка сахара' },
    { text: 'a glass of water', translation: 'стакан воды' },
    { text: 'a packet of cigarettes', translation: 'пачка сигарет' },
    { text: 'a jug of milk', translation: 'кувшин молока' },
    { text: 'a bottle of water', translation: 'бутылка воды' },
    { text: 'a jar of marmalade', translation: 'банка варенья' },
    { text: 'a vase of flowers', translation: 'ваза цветов' },
    { text: 'a piece of cake', translation: 'кусок пирога' },
    { text: 'bar of soap', translation: 'кусок мыла' },
    { text: 'tin of sardines', translation: 'рыбные консервы' },
    { text: 'bowl of cereal', translation: 'тарелка каши' },
    { text: 'packet of biscuits', translation: 'пачка печенья' },
    { text: 'a post office', translation: 'почта' },
    { text: 'hotel', translation: 'отель' },
    { text: 'bus stop', translation: 'автобусная остановка' },
    { text: 'a group of people', translation: 'группа людей' },
    { text: 'a bunch of grapes', translation: 'охапка винограда' },
    { text: 'a bunch of flowers', translation: 'букет цветов' },
    { text: 'a herd of cows', translation: 'стадо коров' },
    { text: 'a gang of youths', translation: 'банда молодежи' },
    { text: 'bit of time', translation: 'малое количество времени' },
];

const furniture = [
    { text: 'chair', translation: 'стул' },
    { text: 'sofa', translation: 'диван' },
    { text: 'bath', translation: 'ванна' },
    { text: 'cooker', translation: 'плита' },
    { text: 'fridge', translation: 'холодильник' },
    { text: 'kitchen', translation: 'кухня' },
    { text: 'toilet', translation: 'туалет' },
    { text: 'living room', translation: 'гомтинная' },
    { text: 'hall', translation: 'зал' },
    { text: 'balcony', translation: 'балкон' },
    { text: 'bedroom', translation: 'спальня' },
    { text: 'armchair', translation: 'кресло' },
    { text: 'cupboard', translation: 'шкафчик' },
    { text: 'coffetable', translation: 'журнальный столик' },
    { text: 'wardrobe', translation: 'шкаф для одежды' },
    { text: 'towel', translation: 'полотенце' },
    { text: 'mirror', translation: 'зеркало' },
    { text: 'fire place', translation: 'камин' },
    { text: 'plant', translation: 'растение' },
    { text: 'carpet', translation: 'ковер' },
    { text: 'rug', translation: 'коврик' },
    { text: 'chelf', translation: 'полка' },
    { text: 'bathroom cabinet', translation: 'ванный шкафчик' },
    { text: 'bookcase', translation: 'книжный шкаф' },
    { text: 'fork', translation: 'вилка' },
    { text: 'knife', translation: 'нож' },
];

const factOrFiction = [
    { text: 'a drama', translation: 'драма' },
    { text: 'a horror film', translation: 'фильм ужасов' },
    { text: 'a thriller', translation: 'триллер' },
    { text: 'a musical', translation: 'мюзикл' },
    { text: 'a disaster movie', translation: 'фильм-катастрофа' },
    { text: 'a documentary', translation: 'документальный фильм' },
    { text: 'a soap opera', translation: 'мыльная опера' },
    { text: 'a cartoon', translation: 'мультфильм' },
    { text: 'a comedy', translation: 'комедия' },
    { text: 'a romance', translation: 'романтика' },
    { text: 'a science fiction(sci-fi) film', translation: 'научная фантастика' },
    { text: 'a historical film', translation: 'исторический фильм' },
    { text: 'an action film', translation: 'боевик' },
    { text: 'an adventure film', translation: 'приключенческий фильм' },
    { text: 'sad', translation: 'грустный' },
    { text: 'serious', translation: 'серьезный' },
    { text: 'frightening', translation: 'странный' },
    { text: 'boring', translation: 'скучный' },
    { text: 'exciting', translation: 'захватывающий' },
    { text: 'happy', translation: 'счастливый' },
    { text: 'romantic', translation: 'романтический' },
    { text: 'slow', translation: 'медленный' },
    { text: 'fast-moving', translation: 'быстроразвивающийся' },
    { text: 'enjoyable', translation: 'интересный' },
    { text: 'funny', translation: 'веселый' },
];

const test = factOrFiction.splice(0, 6);

export const parseToJson = (line) => {
    const lines = line.split(';');
    const words = lines.map((wordData) => {
        const wordLines = wordData.split(':');

        return {
            text: wordLines[0],
            translation: wordLines[1],
        };
    });

    return JSON.stringify(words);
};

export const getSQLiteQueryToInsert = (
    words,
    categoryId,
) => `INSERT INTO word (text, translation, categoryId)
VALUES
  ${words.map(w => `("${w.text}","${w.translation}",${categoryId})`)};`;

export { simpleFood, difficultFood, furniture, factOrFiction, test };
