import SQLite from 'react-native-sqlite-storage';

import CategoryRepo from './category-repository';
import WordRepo from './word-repository';

// SQLite.DEBUG(true);
SQLite.enablePromise(true);

class SQLiteManager {
    constructor() {
        this.db = null;
        this.categoryRepo = null;
        this.wordRepo = null;
    }

    open() {
        return SQLite.openDatabase({ name: 'englishAppDB.db', createFromLocation: 1 }).then(
            (db) => {
                this.db = db;
                this.categoryRepo = new CategoryRepo(db);
                this.wordRepo = new WordRepo(db);

                Promise.resolve();
            },
            error => Promise.reject(error),
        );
    }

    close() {
        this.db.close().then(
            () => {
                Promise.resolve();
            },
            error => Promise.reject(error),
        );
    }
}

export default new SQLiteManager();
