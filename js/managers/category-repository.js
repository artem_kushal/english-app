import { List } from 'immutable';
import CategoryRecord from '../records/category-record';

export default class CategoryRepository {
    constructor(db) {
        this.db = db;
    }

    getAllCategories() {
        return this.db.executeSql('SELECT * FROM category', []).then(
            ([{ rows }]) => {
                let category = new List();

                for (let i = 0; i < rows.length; i += 1) {
                    category = category.push(new CategoryRecord({
                        id: rows.item(i).Id,
                        name: rows.item(i).name,
                        color: rows.item(i).color,
                    }));
                }

                return category;
            },
            error => Promise.reject(error),
        );
    }

    addCategory(name, color) {
        return this.db
            .executeSql(`INSERT INTO category (name, color) VALUES ("${name}", "${color}");`, [])
            .then(
                ([{ insertId }]) =>
                    new CategoryRecord({
                        id: insertId,
                        name,
                        color,
                    }),
                error => Promise.reject(error),
            );
    }

    deleteCategory(categoryId) {
        return this.db
            .executeSql(`DELETE FROM category WHERE Id = ${categoryId};`, [])
            .then(() => Promise.resolve(), error => Promise.reject(error));
    }

    updateCategory(categoryId, name, color) {
        return this.db
            .executeSql(
                `UPDATE category SET name = "${name}", color = "${color}" WHERE Id = ${categoryId};`,
                [],
            )
            .then(
                () =>
                    new CategoryRecord({
                        id: categoryId,
                        name,
                        color,
                    }),
                error => Promise.reject(error),
            );
    }
}
