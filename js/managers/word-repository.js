import { List } from 'immutable';
import WordRecord from '../records/word-record';

export default class CategoryRepository {
    constructor(db) {
        this.db = db;
    }

    getWordsByCategory(categoryId) {
        return this.db.executeSql(`SELECT * FROM word WHERE categoryId=${categoryId}`, []).then(
            ([{ rows }]) => {
                let words = new List();

                for (let i = 0; i < rows.length; i += 1) {
                    words = words.push(new WordRecord({
                        id: rows.item(i).Id,
                        text: rows.item(i).text,
                        translation: rows.item(i).translation,
                        transcription: rows.item(i).transcription,
                    }));
                }

                return words;
            },
            error => Promise.reject(error),
        );
    }

    addWord(categoryId, text, translation, transcription) {
        return this.db
            .executeSql(
                `INSERT INTO word (text, translation, transcription, categoryId) VALUES ("${text}", "${translation}", "${transcription}", ${categoryId});`,
                [],
            )
            .then(
                ([{ insertId }]) =>
                    new WordRecord({
                        id: insertId,
                        text,
                        translation,
                        transcription,
                    }),
                error => Promise.reject(error),
            );
    }

    deleteWords(wordsId) {
        return this.db
            .executeSql(`DELETE FROM word WHERE Id IN (${wordsId.join(',')});`, [])
            .then(() => Promise.resolve(), error => Promise.reject(error));
    }

    updateWord(wordId, text, translation, transcription) {
        return this.db
            .executeSql(
                `UPDATE word
                SET text = "${text}",
                    translation = "${translation}",
                    transcription = "${transcription}"
                WHERE
                    Id = ${wordId};`,
                [],
            )
            .then(
                () =>
                    new WordRecord({
                        id: wordId,
                        text,
                        translation,
                        transcription,
                    }),
                error => Promise.reject(error),
            );
    }
}
