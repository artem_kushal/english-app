import { ActionTypes } from '../constants/category-constants';
import SQLiteManager from '../managers/sqlite-manager';

export const addCategorySuccess = addedCategory => ({
    type: ActionTypes.CATEGORY_ADD_SUCCESS,
    addedCategory,
});

export const addCategory = (name, color) => (dispatch) => {
    SQLiteManager.categoryRepo
        .addCategory(name, color)
        .then(category => dispatch(addCategorySuccess(category)), error => console.log(error));
};

export const removeCategorySuccess = categoryId => ({
    type: ActionTypes.CATEGORY_REMOVE,
    categoryId,
});

export const removeCategory = categoryId => (dispatch) => {
    SQLiteManager.categoryRepo
        .deleteCategory(categoryId)
        .then(category => dispatch(removeCategorySuccess(category)), error => console.log(error));
};

export const editCategorySuccess = updatedCategory => ({
    type: ActionTypes.CATEGORY_EDIT,
    updatedCategory,
});

export const editCategory = (name, color) => (dispatch, getState) => {
    const categoryId = getState().categories.get('editCategoryId');

    SQLiteManager.categoryRepo
        .updateCategory(categoryId, name, color)
        .then(category => dispatch(editCategorySuccess(category)), error => console.log(error));
};

export const successLoadingCategories = categories => ({
    type: ActionTypes.SUCCESS_LOADING_CATEGORIES,
    categories,
});

export const startEditingCategory = categoryId => ({
    type: ActionTypes.CATEGORY_START_EDITING,
    categoryId,
});
