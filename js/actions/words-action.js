import { ActionTypes } from '../constants/words-constants';
import SQLiteManager from '../managers/sqlite-manager';

export const addWordSuccess = addedWord => ({
    type: ActionTypes.ADD_WORD_SUCCESS,
    addedWord,
});

export const addWord = (text, translation, transcription) => (dispatch, getState) => {
    const categoryId = getState().words.get('categoryId');

    SQLiteManager.wordRepo
        .addWord(categoryId, text, translation, transcription)
        .then(word => dispatch(addWordSuccess(word)), error => console.log(error));
};

export const startGettingWords = () => ({
    type: ActionTypes.START_GETTING_WORDS,
});

export const getWordsSuccess = (categoryId, words) => ({
    type: ActionTypes.WORD_GET_BY_CATEGORY_SUCCESS,
    categoryId,
    words,
});

export const getWords = categoryId => (dispatch) => {
    dispatch(startGettingWords);

    SQLiteManager.wordRepo
        .getWordsByCategory(categoryId)
        .then(words => dispatch(getWordsSuccess(categoryId, words)), error => console.log(error));
};

export const deleteWordsSuccess = wordIds => ({
    type: ActionTypes.WORD_DELETE_WORDS,
    wordIds,
});

export const deleteWords = wordIds => (dispatch) => {
    SQLiteManager.wordRepo
        .deleteWords(wordIds)
        .then(() => dispatch(deleteWordsSuccess(wordIds)), error => console.log(error));
};

export const editWordSuccess = updatedWord => ({
    type: ActionTypes.WORD_EDIT,
    updatedWord,
});

export const editWord = (id, text, translation, transcription) => (dispatch) => {
    SQLiteManager.wordRepo
        .updateWord(id, text, translation, transcription)
        .then(word => dispatch(editWordSuccess(word)), error => console.log(error));
};
