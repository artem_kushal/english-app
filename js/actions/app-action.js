import SQLiteManager from '../managers/sqlite-manager';

import { successLoadingCategories } from '../actions/category-action';

export const startApplication = () => (dispatch) => {
    SQLiteManager.open()
        .then(() => SQLiteManager.categoryRepo.getAllCategories())
        .then(
            categories => dispatch(successLoadingCategories(categories)),
            error => console.log(error),
        );
};

export const stopApplication = () => {
    SQLiteManager.close().catch(error => console.log(error));
};
