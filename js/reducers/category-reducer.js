import { List, Record } from 'immutable';

import { ActionTypes } from '../constants/category-constants';

const CategoryStateRecord = Record({
    editCategoryId: null,
    categories: new List(),
});

export default function (state = new CategoryStateRecord(), action) {
    switch (action.type) {
    case ActionTypes.CATEGORY_ADD_SUCCESS:
        return state.update('categories', c => c.push(action.addedCategory));

    case ActionTypes.CATEGORY_REMOVE: {
        const foundIndex = state.get('categories').findIndex(c => c.id === action.categoryId);

        return state.update('categories', c => c.remove(foundIndex));
    }

    case ActionTypes.CATEGORY_EDIT: {
        const { updatedCategory } = action;
        const foundIndex = state
            .get('categories')
            .findIndex(c => c.id === updatedCategory.get('id'));

        return state.setIn(['categories', foundIndex], updatedCategory);
    }
    case ActionTypes.CATEGORY_START_EDITING:
        return state.set('editCategoryId', action.categoryId);

    case ActionTypes.SUCCESS_LOADING_CATEGORIES:
        return state.set('categories', action.categories);

    default:
        return state;
    }
}
