import { Record, List } from 'immutable';

import { ActionTypes } from '../constants/words-constants';

const WordsRecord = Record({
    categoryId: null,
    words: new List(),
});

export default function (state = new WordsRecord(), action) {
    switch (action.type) {
    case ActionTypes.ADD_WORD_SUCCESS:
        return state.update('words', words => words.push(action.addedWord));

    case ActionTypes.START_GETTING_WORDS:
        return new WordsRecord();

    case ActionTypes.WORD_GET_BY_CATEGORY_SUCCESS:
        return new WordsRecord({
            categoryId: action.categoryId,
            words: action.words,
        });

    case ActionTypes.WORD_EDIT: {
        const { updatedWord } = action;
        const foundIndex = state.findIndex(w => w.id === updatedWord.get('id'));

        return state.updateIn(['words', foundIndex], updatedWord);
    }

    case ActionTypes.WORD_DELETE_WORDS: {
        const { wordIds } = action;

        return state.update('words', words =>
            words.filter(w => !wordIds.includes(w.get('id'))));
    }

    default:
        return state;
    }
}
