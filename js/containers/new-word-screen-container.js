import { connect } from 'react-redux';

import { addWord } from '../actions/words-action';

import NewWordScreen from '../components/single-word/single-word-screen';

const mapStateToProps = (state, ownProps) => ({
    toastText: 'Слово добавлено.',
    ...ownProps,
});

const mapDispatchToProps = dispatch => ({
    saveWord: (wordId, text, translation, transcription) => {
        dispatch(addWord(text, translation, transcription));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(NewWordScreen);
