import { connect } from 'react-redux';

import { getWords, deleteWords } from '../actions/words-action';

import WordListScreen from '../components/word-list/word-list-screen';

const mapStateToProps = (state, props) => ({
    words: state.words.get('words'),
    ...props,
});

const mapDispatchToProps = dispatch => ({
    getWords: (categoryId) => {
        dispatch(getWords(categoryId));
    },
    deleteWords: (wordIds) => {
        dispatch(deleteWords(wordIds));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(WordListScreen);
