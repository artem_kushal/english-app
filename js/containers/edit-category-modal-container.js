import { connect } from 'react-redux';

import { editCategory } from '../actions/category-action';

import CategoryModal from '../components/category/category-modal';

const mapStateToProps = (state, props) => {
    const categoryId = state.categories.get('editCategoryId');
    const editCategoryInfo = state.categories.get('categories').find(c => c.id === categoryId);

    return {
        name: editCategoryInfo.get('name'),
        color: editCategoryInfo.get('color'),
        title: 'Изменение категории',
        ...props,
    };
};

const mapDispatchToProps = dispatch => ({
    onSaveCategory: (name, color) => {
        dispatch(editCategory(name, color));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryModal);
