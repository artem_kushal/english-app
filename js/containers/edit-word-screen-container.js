import { connect } from 'react-redux';

import { editWord } from '../actions/words-action';

import NewWordScreen from '../components/single-word/single-word-screen';

const mapStateToProps = (state, props) => {
    const { wordId } = props.navigation.state.params;
    const wordForEditing = state.words.get('words').find(w => w.get('id') === wordId);

    return {
        toastText: 'Изменения сохранены.',

        wordId,
        text: wordForEditing.get('text'),
        translation: wordForEditing.get('translation'),
        transcription: '',

        ...props,
    };
};

const mapDispatchToProps = dispatch => ({
    saveWord: (id, text, translation, transcription) => {
        dispatch(editWord(id, text, translation, transcription));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(NewWordScreen);
