import { connect } from 'react-redux';

import { getWords } from '../actions/words-action';

import { shuffleList } from '../tools/list-tools';

import ExerciseScreen from '../components/exercise/exercise-screen';

const mapStateToProps = state => ({
    words: shuffleList(state.words.get('words')),
});

const mapDispatchToProps = dispatch => ({
    getWords: (categoryId) => {
        dispatch(getWords(categoryId));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(ExerciseScreen);
