import { connect } from 'react-redux';

import { startApplication, stopApplication } from '../actions/app-action';

import App from '../app';

const mapDispatchToProps = dispatch => ({
    onStartApplication: () => {
        dispatch(startApplication());
    },
    onStopApplication: () => {
        dispatch(stopApplication());
    },
});

export default connect(() => ({}), mapDispatchToProps)(App);
