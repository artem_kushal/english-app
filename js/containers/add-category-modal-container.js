import { connect } from 'react-redux';

import { addCategory } from '../actions/category-action';

import CategoryModal from '../components/category/category-modal';

const mapStateToProps = props => ({
    title: 'Новая категория',
    ...props,
});

const mapDispatchToProps = dispatch => ({
    onSaveCategory: (name, color) => {
        dispatch(addCategory(name, color));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryModal);
