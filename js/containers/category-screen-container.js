import { connect } from 'react-redux';

import { removeCategory, startEditingCategory } from '../actions/category-action';

import CategoryScreen from '../components/category/category-screen';

const mapStateToProps = (state, props) => ({
    categories: state.categories.get('categories'),
    ...props,
});

const mapDispatchToProps = dispatch => ({
    onRemoveCategory: (categoryId) => {
        dispatch(removeCategory(categoryId));
    },
    onStartEditingCategory: (categoryId) => {
        dispatch(startEditingCategory(categoryId));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryScreen);
