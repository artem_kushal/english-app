export const DARK_PRIMARY_COLOR = '#1976D2';
export const PRIMARY_COLOR = '#2196F3';
export const LIGHT_PRIMARY_COLOR = '#BBDEFB';
export const TEXT_PRIMARY_COLOR = '#FFFFFF';
export const ACCENT_COLOR = '#FFC107';
export const PRIMARY_TEXT_COLOR = '#212121';
export const SECONDARY_TEXT_COLOR = '#757575';
export const DIVIDER_COLOR = '#BDBDBD';
