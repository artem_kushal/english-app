import React from 'react';

import Icon from 'react-native-vector-icons/MaterialIcons';

import NewWordScreen from '../containers/new-word-screen-container';
import EditWordScreen from '../containers/edit-word-screen-container';
import WordListScreen from '../containers/word-list-screen-container';
import CategoryScreen from '../containers/category-screen-container';
import ExerciseScreen from '../containers/exersize-screen-container';

export const RoutesID = {
    NEW_WORD: 'NEW_WORD',
    EDIT_WORD: 'EDIT_WORD',
    WORD_LIST: 'WORD_LIST',
    CATEGORY: 'CATEGORY',
    EXERCISE: 'EXERCISE',
};

export const NavigationOptions = {
    [RoutesID.CATEGORY]: {
        screen: props => <CategoryScreen {...props} title="Словарь" />,
        navigationOptions: {
            drawerLabel: 'Категории',
            drawerIcon: ({ tintColor }) => (
                <Icon name="local-library" color={tintColor} size={22} />
            ),
        },
    },
    [RoutesID.NEW_WORD]: {
        screen: props => <NewWordScreen {...props} title="Новое слово" />,
        navigationOptions: {
            header: null,
            drawerLabel: () => null,
        },
    },
    [RoutesID.EDIT_WORD]: {
        screen: props => <EditWordScreen {...props} title="Редактирование" />,
        navigationOptions: {
            header: null,
            drawerLabel: () => null,
        },
    },
    [RoutesID.WORD_LIST]: {
        screen: props => <WordListScreen {...props} title="Новое Слова" />,
        navigationOptions: {
            header: null,
            drawerLabel: () => null,
        },
    },
    [RoutesID.EXERCISE]: {
        screen: props => <ExerciseScreen {...props} title="" />,
        navigationOptions: {
            header: null,
            drawerLabel: () => null,
        },
    },
};

// navigationOptions: {
//     title: 'Словарь',
//     headerTitleStyle: {
//         color: '#fff',
//     },
// },
