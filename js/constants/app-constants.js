export const Types = {
    START_APP: 'START_APP',
    STOP_APP: 'STOP_APP',
};

export const MIN_WORDS_TO_EXERSIZE = 5;
