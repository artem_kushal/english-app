import { List } from 'immutable';

/**
 * http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
 * Randomize array element order in-place.
 * Using Durstenfeld shuffle algorithm.
 */

export const shuffleList = (list) => {
    let shuffledList = list;

    for (let i = list.count() - 1; i > 0; i -= 1) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = shuffledList.get(i);

        shuffledList = shuffledList.set(i, shuffledList.get(j));
        shuffledList = shuffledList.set(j, temp);
    }

    return shuffledList;
};

export const generateUniqRandList = (list, count) => {
    const randIndexes = [];

    while (randIndexes.length < count) {
        const randIndex = Math.floor(Math.random() * list.count());

        if (!randIndexes.includes(randIndex)) {
            randIndexes.push(randIndex);
        }
    }

    return new List(randIndexes.map(i => list.get(i)));
};
