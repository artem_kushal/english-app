import { Record } from 'immutable';

export default Record({
    id: null,
    text: null,
    translation: null,
    transcription: null,
});
