import React from 'react';
import PropTypes from 'prop-types';
import { DrawerNavigator } from 'react-navigation';

import { NavigationOptions } from './constants/navigation-constants';

const RootNavigator = DrawerNavigator(NavigationOptions, {
    drawerWidth: 250,
    drawerPosition: 'left',
    contentOptions: {
        inactiveTintColor: 'darkgray',
    },

    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
});

export default class App extends React.Component {
    static propTypes = {
        onStartApplication: PropTypes.func.isRequired,
        onStopApplication: PropTypes.func.isRequired,
    };

    componentWillMount() {
        this.props.onStartApplication();
    }

    componentWillUnmount() {
        this.props.onStopApplication();
    }

    render() {
        return <RootNavigator />;
    }
}

/*
  с помощью API DIMENSION смотреть размер экрана и расчитывать ширину контейнера,
  для карточек(смотреть сколько может поместитьс на экран и в зависимости от этого ставить ширину)
*/
